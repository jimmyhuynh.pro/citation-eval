-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Jimmy
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2022-02-22 20:35
-- Created:       2022-02-21 10:02
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "auteur"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45) NOT NULL,
  "createdAT" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "publish" BOOLEAN NOT NULL DEFAULT 0
);
CREATE TABLE "citation"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "quote" LONG VARCHAR NOT NULL,
  "auteur_id" INTEGER NOT NULL,
  "createdAT" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "fk_citation_auteur"
    FOREIGN KEY("auteur_id")
    REFERENCES "auteur"("id")
);
CREATE INDEX "citation.fk_citation_auteur_idx" ON "citation" ("auteur_id");
COMMIT;






