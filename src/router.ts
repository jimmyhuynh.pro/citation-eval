import { Application } from "express";
import CitationController from "./controllers/CitationController";
import AuteurController from "./controllers/AuteurController";

export default function route(app: Application)
{
    /** Static pages Citations **/
    app.get('/', (req, res) =>
    {
        CitationController.index(req, res);
    });

    app.get('/citation-all', (req, res) =>
    {
        CitationController.citationList(req, res);
    });

    app.get('/citation-create', (req, res) =>
    {
        CitationController.showForm(req, res)
    });

    app.post('/citation-create', (req, res) =>
    {
        CitationController.create(req, res)
    });

    app.get('/citation-read/:id', (req, res) =>
    {
        CitationController.read(req, res)
    });

    app.get('/citation-update/:id', (req, res) =>
    {
        CitationController.showFormUpdate(req, res)
    });

    app.post('/citation-update/:id', (req, res) =>
    {
        CitationController.update(req, res)
    });

    app.get('/citation-delete/:id', (req, res) =>
    {
        CitationController.delete(req, res)
    });


    /** Static pages Auteurs **/

    app.get('/admin', (req, res) =>
    {
        AuteurController.index(req, res)
    });

    app.get('/auteur-all', (req, res) =>
    {
        AuteurController.citationList(req, res);
    });

    app.get('/auteur-create', (req, res) =>
    {
        AuteurController.showForm(req, res);
    });

    app.post('/auteur-create', (req, res) =>
    {
        AuteurController.create(req, res)
    });

    app.get('/auteur-read/:id', (req, res) =>
    {
        AuteurController.read(req, res)
    });

    app.get('/auteur-update/:id', (req, res) =>
    {
        AuteurController.showFormUpdate(req, res)
    });

    app.post('/auteur-update/:id', (req, res) =>
    {
        AuteurController.update(req, res)
    });

    app.get('/auteur-delete/:id', (req, res) =>
    {
        AuteurController.delete(req, res)
    });
}

