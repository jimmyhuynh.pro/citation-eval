import { Request, Response } from "express-serve-static-core";
import { db } from "../server";


export default class CitationController {

    static index(req: Request, res: Response) {

        const citations = db.prepare('SELECT * FROM citation WHERE id=(SELECT max(id) FROM citation)').get();
        const auteurs = db.prepare('SELECT * FROM auteur').all();

        console.log(citations);

        res.render('pages/index', {
            title : 'Citation',
            citations: citations,
            auteurs: auteurs
        });
    }


    // Affiche la liste des citations
    static citationList(req: Request, res: Response) {

        const citations = db.prepare('SELECT * FROM citation').all();
        const auteurs = db.prepare('SELECT * FROM auteur').all();

        res.render('pages/citation-all', {
            title: 'Les citations',
            citations: citations,
            auteurs: auteurs
        });
    }

    // Affiche le formulaire de creation de citation

    static showForm(req: Request, res: Response): void {

        res.render('pages/citation-create')
    }

    // Recupere le formulaire et insere la citation en db

    static create(req: Request, res: Response): void {


        const citations = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id);
        const auteurs = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);

        db.prepare('INSERT INTO citation ("quote", "auteur_id") VALUES (?,?)').run(req.body.title, req.body.user_id);
        db.prepare('UPDATE auteur SET publish=1 WHERE id=?').run(req.body.user_id);

        
        // res.render('pages/citation-create', {
        //     title: 'Les citations',
        //     citations: citations,
        //     auteurs: auteurs
        // });

        CitationController.index(req, res)
    }

    // Affiche une citation

    static read(req: Request, res: Response): void {

        const citations = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id);
        const auteurs = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);

        res.render('pages/citation-read', {
            citation: citations,
            auteurs: auteurs
        });
    }

    // Affiche le formulaire pour modifier une citation

    static showFormUpdate(req: Request, res: Response) {

        const citations = db.prepare('SELECT * FROM citation WHERE id = ?').get(req.params.id);
        const auteurs = db.prepare('SELECT * FROM auteur').all();

        res.render('pages/citation-update', {
            citation: citations,
            auteurs: auteurs
        });
    }

    // Recupere le formulaire de la citation modifié et l'ajoute à la database

    static update(req: Request, res: Response) {

        db.prepare('UPDATE citation SET quote = ?, auteur_id = ? WHERE id = ?').run(req.body.title, req.params.id, 1);

        CitationController.index(req, res);
    }

    // Supprimer une citation

    static delete(req: Request, res: Response) {

        const citationDelete = db.prepare('DELETE FROM citation WHERE id = ?').run(req.params.id);

        CitationController.index(req, res);
    }


}
