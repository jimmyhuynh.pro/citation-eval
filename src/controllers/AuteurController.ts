import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class AuteurController {

    //Affiche la liste des auteurs et citations
    static index(req: Request, res: Response) {

        const auteurs = db.prepare('SELECT * FROM auteur').all();
        const citations = db.prepare('SELECT * FROM citation').all();

        res.render('pages/admin', {
            title: 'Liste',
            auteurs: auteurs,
            citations: citations,
        });
    }

    //affiche la liste des auteurs et citations
    static citationList(req: Request, res: Response) {

        const auteurs = db.prepare('SELECT * FROM auteur WHERE publish=1').all();
        const citations = db.prepare('SELECT * FROM citation').all();

        res.render('pages/auteur-all', {
            title: 'Liste',
            auteurs: auteurs,
            citations: citations
        })
    }

    // Affiche le formulaire de creation des auteurs

    static showForm(req: Request, res: Response): void {

        res.render('pages/auteur-create')
    }

    // Recupere le formulaire et insere l'auteur en db

    static create(req: Request, res: Response): void {

        let addAuteur = db.prepare('INSERT INTO auteur("name") VALUES (?)').run(req.body.title);

        AuteurController.index(req, res)
    }

    // Affiche les auteurs

    static read(req: Request, res: Response): void {

        const auteurs = db.prepare('SELECT * FROM auteur WHERE id = ?').get(req.params.id);
        const citations = db.prepare('SELECT * FROM citation WHERE auteur_id = ?').all(req.params.id)

        res.render('pages/auteur-read', {
            auteurs: auteurs,
            citations: citations
        });
    }

    // Affiche le formulaire pour modifier un auteur

    static showFormUpdate(req: Request, res: Response) {

        const auteurs = db.prepare('SELECT * FROM auteur WHERE id= ?').get(req.params.id);

        res.render('pages/auteur-update', {
            auteurs: auteurs
        });
    }

    // Recupere le formulaire de l'auteur modifié et l'ajoute à la database

    static update(req: Request, res: Response) {

        const auteurs = db.prepare('UPDATE auteur SET name = ? WHERE id = ?').run(req.body.title, req.params.id);


        AuteurController.index(req, res);
    }

    // Supprimer un auteur

    static delete(req: Request, res: Response) {

        const auteurDelete = db.prepare('DELETE FROM auteur WHERE id = ?').run(req.params.id);
        const citationDelete = db.prepare('DELETE FROM citation WHERE id = ?').run(req.params.id);
    
        AuteurController.index(req, res);
    }

}
